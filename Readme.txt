STATE MACHINE TEMPERATURE CONTROL

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 

Design a state machine, which meets the requirements of
having a temperature control, a door access system and a
system to visualize that the system is on.

PLATAFORM.
The board STM32F429ZI discovery 1 will be used on this development. 

FUNCTIONALITY.

* Door system --> The door activation only consists of one press of 
the user button, since the door opening is programmed to be open 
for 3 seconds and then closes and active the ALERT LED.

* Heartbeat --> This mode implemente the LED on the GPIO G13 that
indicate the system is ON with a blink that turn on and off per second. 

* Temperature control: 

Heater --> this actuator is active (ON) when the system gets a temperature under 10°C. 
If the temperature is over 12°C the actuator is desactive (OFF).

Fan --> This actuator is controlled by a PWM signal. This PWM signal can vary in different duty cycles depending 
on the sensed temperature.  If the temperature is between 20°C to under 28° C, the PWM is on the 20% of the signal's
duty cycle.  If the temperature is between 30°C to under 38°C the PWM signal is on 60% of the duty cycle. And finally,
if the temperature is between 40°C to under 45°C the PWM is on 100% of the duty cycle. The PWM is generated on the timer 1 
of the board, and used on the DMA peripheral to produce a smooth change of the steps on the changing of the duty cycle. 

Alert --> This actuator has control ON/OFF. And is active (ON) when the temperature is over 45°C and under 0°C. 
In the mode of ALERT!, the system put OFF all the actuators (heater and Fan). This actuator is simulate on the LED on
GPIO G14.  