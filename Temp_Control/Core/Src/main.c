/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#define Buffer_PWM_size 1024
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

////////////////// Structure for the bmp280	Sensor register address/////////////////////////////////////////7
typedef enum bmp280_register_address_ {

BMP280_REG_ADDR_ERR = 0x00,
BMP280_REG_ADDR_CAL_START_ADDR = 0x88,
BMP280_REG_ADDR_ID = 0xD0,
BMP280_REG_ADDR_RESET = 0xE0,
BMP280_REG_ADDR_STATUS = 0xF3,
BMP280_REG_ADDR_CTRL_MEAS = 0xF4,
BMP280_REG_ADDR_CONFIG = 0xF5,
BMP280_REG_ADDR_PRESS_MSB = 0xF7,
BMP280_REG_ADDR_PRESS_LBS = 0xF8,
BMP280_REG_ADDR_PRESS_XLBS = 0xF9,
BMP280_REG_ADDR_TEMP_MSB = 0xFA,
BMP280_REG_ADDR_TEMP_LBS = 0xFB,
BMP280_REG_ADDR_TEMP_XLBS = 0xFC,

} bmp280_register_address_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
SPI_HandleTypeDef hspi1;
TIM_HandleTypeDef htim1;
DMA_HandleTypeDef hdma_tim1_ch1;
UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void door(void)	// detection of the User button to active or disactive the door
{
	uint16_t adc_val =0;
	if (HAL_ADC_PollForConversion(&hadc1, 10) == HAL_OK){
	 adc_val = HAL_ADC_GetValue(&hadc1);

	 if(adc_val > 3900){
		 HAL_GPIO_WritePin(Door_GPIO_Port, Door_Pin, GPIO_PIN_SET);
		 HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_SET);
		 printf("Door is open \r\n");
		 HAL_Delay(3000);
		 printf("Door is close \r\n");
		 HAL_GPIO_WritePin(Door_GPIO_Port, Door_Pin, GPIO_PIN_RESET);
		 HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);

	 }

	}
}

void heartbeat(void) //   Heartbeat for the system
{
	static uint32_t tick = 0;
	if(tick < HAL_GetTick()){
		tick = HAL_GetTick () + 1000;  // Get the period of 1000 ms for the toggle LED.
		HAL_ADC_Start(&hadc1);
		HAL_GPIO_TogglePin(Heartbeat_GPIO_Port, Heartbeat_Pin);
	}
}

float temp_meas(uint32_t toggle_period_ms, uint8_t toogle_meas_enable )	       // read the sensor and take the temperature in degrees
{

	uint32_t meas_tick = 0;
	int32_t raw_temperature = 0;
	uint8_t spi_tx_data_meas_reg = 0x00;
	uint8_t spi_rx_meas_data[6] = { 0x00 };

	float temp_degr = 0;
	double var1 = 0;
	double var2 = 0;

	const uint16_t dig_T1 = 0b0110100110100011;
	const int16_t dig_T2 = 0b0110101001000000;
	const int16_t dig_T3 = 0b1111110000011000;

	if (toogle_meas_enable) {
				if ((HAL_GetTick() - meas_tick) > toggle_period_ms) {
					meas_tick = HAL_GetTick();				// Evaluate the period

					///////////////// Take the temperature measurement /////////////////////////////////////////
					spi_tx_data_meas_reg = BMP280_REG_ADDR_TEMP_MSB;
					HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin,GPIO_PIN_RESET);
					if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_meas_reg, 1, 10) == HAL_OK) {
						if (HAL_SPI_Receive(&hspi1, spi_rx_meas_data, 1, 10) == HAL_OK) {
							HAL_Delay(10);
							raw_temperature =(int32_t) (spi_rx_meas_data[2] & 0b00001111) | (spi_rx_meas_data[1] << 4) | (spi_rx_meas_data[0] << 12);

							/////////////////////////////////////// Implementation of the transformation equation /////////////////////////////////
							var1 = (((double) raw_temperature) / 16384.0 - ((double) dig_T1) / 1024.0) * ((double) dig_T2);
							var2 = ((double) raw_temperature) / 131072.0 - ((double) dig_T1) / 8192.0;
							var2 = (var2 * var2) * ((double) dig_T3);
							temp_degr = (float) (var1 + var2) / 5120.0;

							/////////////////////////////////////////////// Send the value of the degrees on the UART /////////////////////////////////
							printf("Temp: %.2f",temp_degr);
							printf("°C\r\n");
						  }
					  }
						HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin,GPIO_PIN_SET);
						HAL_Delay(1000);
					 }
				  }
	return temp_degr;
}

void PWM_DMA(float lim_initial, float lim_final)							  // Produce the PWM signal
{
	uint16_t pwm_dma[Buffer_PWM_size] = { 0 };								  // Buffer for PWM data
	const float step = (1023.0 * lim_final - 1023.0 * lim_initial )/ 1023.0;  // calculate the value of the step of each iteration

	for (uint16_t i = 0; i < Buffer_PWM_size; i++) { 						  // PWM of DMA was initialized
		pwm_dma[i] = (int)(1023.0 * lim_initial)+ (int) (step * i); 		  // save the value of the PWM in the actual step
	}

	HAL_TIM_PWM_Start_DMA(&htim1, TIM_CHANNEL_1, (uint32_t*) pwm_dma,Buffer_PWM_size);	// Put the buffer on the DMA to the timer PWM
}

int _write(int file, char *ptr, int len) // Use the Wirte function to the UART1
{
	HAL_UART_Transmit(&huart1,(uint8_t *)ptr, len, 10);
	return len;
}

/* USER CODE END 0 */

/**
 *
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  ////////////////////// Variables ///////////////////////

  uint8_t spi_tx_data_reg = 0x00;			// Address of the registers for the bmp280 sensor
  uint8_t spi_rx_data[6] = { 0x00 };		// Buffer of the sensor
  uint8_t toogle_meas_enable = 1;			// enable the sensor
  uint32_t toggle_period_ms = 1000;			// Period of the reading temperature

  char state = 0;							// Initial state of the state machine
  float temp = 0.0;							// Temperature in degrees



  ///////////////////////////////////////// Configuration of the BMP280 temperature sensor ///////////////////////////////
  // Search the sensor ID
  spi_tx_data_reg = BMP280_REG_ADDR_ID;
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
  if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 10) == HAL_OK) {
  	if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 10) == HAL_OK) {
  		HAL_Delay(10);
  	}
  }
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);

  // Change the Config sensor (temp over samp x2, skipped the press meas, normal power mode)
  uint8_t ctrl_meas_Bytes = 0x23;
  uint8_t spi_config[2] = { 0x74, ctrl_meas_Bytes };

  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
  if (HAL_SPI_Transmit(&hspi1, spi_config, 2, 10) == HAL_OK) {
  	HAL_Delay(10);
  }
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);

  // Verify the CTRL_MEAS register configuration
  spi_tx_data_reg = BMP280_REG_ADDR_CTRL_MEAS;
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);
  if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_reg, 1, 10) == HAL_OK) {
  	if (HAL_SPI_Receive(&hspi1, spi_rx_data, 1, 10) == HAL_OK) {
  		HAL_Delay(10);
  	}
  }
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_SET);
  HAL_Delay(10);

  printf("Temperature system control\r\n");


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		heartbeat();													// System was initialized
		door();															// function of open the door
		temp = temp_meas(toggle_period_ms, toogle_meas_enable);		    // Take the measurement of the temperature sensor

		switch(state){ 													// State Machine was initialized
		case 0: // COLD
			if (temp > 20.0){
				state = 2;
				break;
			} else if (temp < 10.0){
				state = 1;
				break;
			}
			printf("COLD\r\n");
			printf("--> Heater OFF\r\n");
			printf("--> Fan OFF\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_RESET);	// Heater OF
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);		// Alert OFF
			PWM_DMA(0.0, 0.0);													// Fan OFF

			break;
		case 1: //VERY COLD
			if (temp > 12.0){
				state = 0;
				break;
			} else if (temp < 0.0){
				state = 3;
				break;
			}

			printf("VERY COLD\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_SET);		// Heater ON
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);		// Alert OFF
			PWM_DMA(0.0, 0.0);													// Fan OFF
			printf("--> Heater ON\r\n");
			printf("--> Fan OFF\r\n");

			break;
		case 2: //MILD
			if (temp > 30.0){
					state = 4;
					break;
				} else if (temp < 18.0){
					state = 0;
					break;
				}
			printf("MILD\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_RESET);	// Heater OF
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);		// Alert OFF
			PWM_DMA(0.0, 0.2);													// Fan at 20% ON
			printf("--> Heater OFF\r\n");
			printf("--> Fan on 20 prct \r\n");
			HAL_Delay(100);
			break;
		case 3: // ALERT

			printf("ALERT !!\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_RESET);	// Heater OF
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_SET);		// Alert ON
			PWM_DMA(0.0, 0.0);													// Fan OFF
			printf("--> Heater OFF\r\n");
			printf("--> Fan OFF \r\n");

			if (temp > 0.0){
				state = 1;
				break;
			} if (temp < 45.0){
				state = 5;
				break;
			}

			break;

		case 4: //HOT
			if (temp > 40.0){
				state = 5;
				break;
			} else if (temp < 28.0){
				state = 2;
				break;
			}
			printf("HOT\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_RESET);	// Heater OF
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);		// Alert OFF
			PWM_DMA(0.2, 0.6);													// Fan at 60% ON
			printf("--> Heater OFF\r\n");
			printf("--> Fan on 60 prct \r\n");

			break;
		case 5:	// VERY HOT
			if (temp >= 45.0){
				state = 3;
				break;
			} else if (temp < 38.0){
				state = 4;
				break;
			}
			printf("VERY HOT\r\n");
			HAL_GPIO_WritePin(Heater_GPIO_Port, Heater_Pin, GPIO_PIN_RESET);	// Heater OF
			HAL_GPIO_WritePin(Alert_GPIO_Port, Alert_Pin, GPIO_PIN_RESET);		// Alert OFF
			PWM_DMA(0.6, 1);													// Fan at 100% ON
			printf("--> Heater OFF\r\n");
			printf("--> Fan on 100 prct \r\n");

			break;
		default:
			// do nothing
		break;

		printf("\r\n");
		}
		HAL_Delay(100);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 1.5;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(bmp280_CS_GPIO_Port, bmp280_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, Heater_Pin|Door_Pin|Heartbeat_Pin|Alert_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : bmp280_CS_Pin */
  GPIO_InitStruct.Pin = bmp280_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(bmp280_CS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Heater_Pin Door_Pin Heartbeat_Pin Alert_Pin */
  GPIO_InitStruct.Pin = Heater_Pin|Door_Pin|Heartbeat_Pin|Alert_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
